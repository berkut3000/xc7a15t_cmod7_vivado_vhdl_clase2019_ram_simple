LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY TB_RAM_SIMPLE IS
END TB_RAM_SIMPLE;
 
ARCHITECTURE behavior OF TB_RAM_SIMPLE IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT RAM_SIMPLE
    PORT(
         CLK : IN  std_logic;
         DIN : IN  std_logic_vector(7 downto 0);
         ADR : IN  std_logic_vector(2 downto 0);
         WE  : IN  std_logic;
         DOUT: OUT std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal CLK : std_logic := '0';
   signal DIN : std_logic_vector(7 downto 0) := (others => '0');
   signal ADR : std_logic_vector(2 downto 0) := (others => '0');
   signal WE : std_logic := '0';

 	--Outputs
   signal DOUT : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant CLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: RAM_SIMPLE PORT MAP (
          CLK => CLK,
          DIN => DIN,
          ADR => ADR,
          WE => WE,
          DOUT => DOUT
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for CLK_period*10;

      -- insert stimulus here
      WE <= '0';                 -- Deshabilita escritura
      DIN <= "10101010";        -- Asigna valor a DIN
      ADR <= "000";             -- Define direccion de escritura
      wait for 30 ns;            -- Espera 5 ns
      WE <= '1';                -- Habilita escritura
      wait for 10 ns;           -- Espera 1 cilco de reloj para escritura
      WE <= '0';                -- Deshabilita la escritura
      
      -- Guardaremos en otra localida de memoria, copiamos y pegamos lo de arriba
      
      DIN <= "11110010";        --Asigna valor a DIN
      ADR <= "001";             -- Define direccion de escritura
      wait for 30 ns;            -- Espera 5 ns
      WE <= '1';                -- Habilita escritura
      wait for 10 ns;           -- Espera 1 cilco de reloj para escritura
      WE <= '0';                -- Deshabilita la escritura
      

      wait;
   end process;

END;
