----------------------------------------------------------------------------------
-- JORGE SOTO
-- Programa para demostrar el uso de RAM simple del FPGA
-- 1K 18-bits RAM distribuida de un solo puerto
----------------------------------------------------------------------------------
-- Librerias:
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity RAM_SIMPLE is
	generic( Nbits_data : integer := 8;  -- Constante VHDL
			   Nbits_adr : integer := 3);
   
	Port (  CLK : in  STD_LOGIC;
           DIN : in  STD_LOGIC_VECTOR (Nbits_data-1 downto 0);
           ADR : in  STD_LOGIC_VECTOR (Nbits_adr-1 downto 0);
           WE  : in  STD_LOGIC;
           DOUT : out  STD_LOGIC_VECTOR (Nbits_data-1 downto 0));
--attribute ram_style : string;
--attribute ram_style of RAM_SIMPLE : entity is "distributed"; -- "block"	usa slices		  
end RAM_SIMPLE;

architecture Behavioral of RAM_SIMPLE is

type MEM_TYPE is array((2**Nbits_adr)-1 downto 0) of std_logic_vector(DIN'range);
-- Genera un tipo de dato arreglo de 1024 x 18  (2^10 x 18) o (2^Nbits_adr x Nbits_data)
-- ** Potencia 2^10     (DIN'range) = (Nbits_data-1 downto 0)
signal MEM : MEM_TYPE;  -- Define la variable MEM del tipo de dato MEM_TYPE

begin

process(CLK)  begin
	if rising_edge(CLK)  then
		if WE = '1'  then
			MEM(conv_integer(ADR)) <= DIN;
		end if;
		DOUT <= MEM(conv_integer(ADR));
	end if;
end process;	

end Behavioral;
